openapi: 3.0.0
info:
  title: User API
  version: 1.0.0
  description: |-
    This API defines CRUD operations for managing user data. It is a sample User Server based on the OpenAPI 3.0 specification.

    Some useful links:
    - [The User repository](https://github.com/swagger-api/swagger-petstore)

    The User API allows you to perform operations such as creating, updating, retrieving, and deleting user information. It follows the principles of RESTful architecture.

    For more information about Swagger, visit [Swagger's official website](http://swagger.io).

  termsOfService: http://swagger.io/terms/
  contact:
    email: apiteam@swagger.io
  license:
    name: Apache 2.0
    url: http://www.apache.org/licenses/LICENSE-2.0.html
externalDocs:
  description: Find out more about Swagger
  url: http://swagger.io
tags:
  - name: User
    description: Everything about users.
paths:
  /users:
    get:
      tags:
        - User
      summary: Get all users
      responses:
        '200':
          description: Successful response
          content:
            application/json:
              example:
                - id: 1
                  firstName: John
                  lastName: Doe
                  age: 25
                  pesel: '12345678901'
                  nationality: PL
                  email: john.doe@example.com
                - id: 2
                  firstName: Jane
                  lastName: Smith
                  age: 30
                  pesel: '98765432109'
                  nationality: DE
                  email: jane.smith@example.com
          headers:
            x-request-id:
              description: Unique identifier for the request
              schema:
                type: string
                example: abc123def456
            x-request-date:
              description: Date and time when the request was sent
              schema:
                type: string
                format: date-time
                example: '2023-01-01T12:34:56Z'
      security:
        - basicAuth: []
    post:
      tags:
        - User
      summary: Create a new user
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/UserNoId'
      responses:
        '201':
          description: User created successfully
          content:
            application/json:
              example:
                id: 3
                firstName: NewUser
                lastName: NewLastName
                age: 28
                pesel: '65432109876'
                nationality: UK
                email: new.user@example.com
          headers:
            x-request-id:
              description: Unique identifier for the request
              schema:
                type: string
                example: abc123def456
            x-request-date:
              description: Date and time when the request was sent
              schema:
                type: string
                format: date-time
                example: '2023-01-01T12:34:56Z'
        '400':
          description: Bad Request
          content:
            application/json:
              example:
                error: Invalid request body
          headers:
            x-request-id:
              description: Unique identifier for the request
              schema:
                type: string
                example: abc123def456
            x-request-date:
              description: Date and time when the request was sent
              schema:
                type: string
                format: date-time
                example: '2023-01-01T12:34:56Z'
      security:
        - basicAuth: []
  /users/{userId}:
    parameters:
      - name: userId
        in: path
        required: true
        description: ID of the user
        schema:
          type: integer
    get:
      tags:
        - User
      summary: Get a specific user
      responses:
        '200':
          description: Successful response
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
          headers:
            x-request-id:
              description: Unique identifier for the request
              schema:
                type: string
                example: abc123def456
            x-request-date:
              description: Date and time when the request was sent
              schema:
                type: string
                format: date-time
                example: '2023-01-01T12:34:56Z'
        '404':
          description: User not found
          content:
            application/json:
              example:
                error: User not found
          headers:
            x-request-id:
              description: Unique identifier for the request
              schema:
                type: string
                example: abc123def456
            x-request-date:
              description: Date and time when the request was sent
              schema:
                type: string
                format: date-time
                example: '2023-01-01T12:34:56Z'
      security:
        - bearerAuth: []
    put:
      tags:
        - User
      summary: Update a user
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/UserNoId'
      responses:
        '200':
          description: User updated successfully
          content:
            application/json:
              example:
                id: 3
                firstName: NewUser
                lastName: NewLastName
                age: 28
                pesel: '65432109876'
                nationality: UK
                email: new.user@example.com
          headers:
            x-request-id:
              description: Unique identifier for the request
              schema:
                type: string
                example: abc123def456
            x-request-date:
              description: Date and time when the request was sent
              schema:
                type: string
                format: date-time
                example: '2023-01-01T12:34:56Z'
        '400':
          description: Bad Request
          content:
            application/json:
              example:
                error: Invalid request body
          headers:
            x-request-id:
              description: Unique identifier for the request
              schema:
                type: string
                example: abc123def456
            x-request-date:
              description: Date and time when the request was sent
              schema:
                type: string
                format: date-time
                example: '2023-01-01T12:34:56Z'
        '404':
          description: User not found
          content:
            application/json:
              example:
                error: User not found
          headers:
            x-request-id:
              description: Unique identifier for the request
              schema:
                type: string
                example: abc123def456
            x-request-date:
              description: Date and time when the request was sent
              schema:
                type: string
                format: date-time
                example: '2023-01-01T12:34:56Z'
      security:
        - bearerAuth: []
    delete:
      tags:
        - User
      summary: Delete a user
      responses:
        '204':
          description: User deleted successfully
          headers:
            x-request-id:
              description: Unique identifier for the request
              schema:
                type: string
                example: abc123def456
            x-request-date:
              description: Date and time when the request was sent
              schema:
                type: string
                format: date-time
                example: '2023-01-01T12:34:56Z'
        '404':
          description: User not found
          content:
            application/json:
              example:
                error: User not found
          headers:
            x-request-id:
              description: Unique identifier for the request
              schema:
                type: string
                example: abc123def456
            x-request-date:
              description: Date and time when the request was sent
              schema:
                type: string
                format: date-time
                example: '2023-01-01T12:34:56Z'
      security:
        - bearerAuth: []
components:
  schemas:
    User:
      type: object
      properties:
        id:
          type: integer
          format: int64
          example: 1
          readOnly: true  # This indicates that the 'id' field should be read-only
        firstName:
          type: string
          maxLength: 50
          example: John
        lastName:
          type: string
          maxLength: 50
          example: Doe
        age:
          type: integer
          minimum: 0
          example: 25
        pesel:
          type: string
          pattern: '^\d{11}$'
          example: '12345678901'
        nationality:
          type: string
          enum: ['PL', 'DE', 'UK']
          example: 'PL'
        email:
          type: string
          format: email
          example: john.doe@example.com
          pattern: /^.+[@].+[.].+$/
      required:
        - firstName
        - lastName
        - pesel
        - nationality
        - email
    UserNoId:
      type: object
      properties:
        firstName:
          type: string
          maxLength: 50
          example: NewUser
        lastName:
          type: string
          maxLength: 50
          example: NewLastName
        age:
          type: integer
          minimum: 0
          example: 28
        pesel:
          type: string
          pattern: '^\d{11}$'
          example: '65432109876'
        nationality:
          type: string
          enum: ['PL', 'DE', 'UK']
          example: 'UK'
        email:
          type: string
          format: email
          example: new.user@example.com
          pattern: /^.+[@].+[.].+$/
      required:
        - firstName
        - lastName
        - pesel
        - nationality
        - email
  securitySchemes:
    basicAuth:
      type: http
      scheme: basic
      description: HTTP Basic Authentication
    bearerAuth:
      type: http
      scheme: bearer
      description: Bearer Token Authentication
      bearerFormat: JWT
